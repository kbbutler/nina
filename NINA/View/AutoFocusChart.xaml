﻿<!--
    Copyright © 2016 - 2019 Stefan Berg <isbeorn86+NINA@googlemail.com>

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    N.I.N.A. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    N.I.N.A. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with N.I.N.A..  If not, see http://www.gnu.org/licenses/.-->
<UserControl
    x:Class="NINA.View.AutoFocusChart"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:local="clr-namespace:NINA.View"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:ns="clr-namespace:NINA.Locale"
    xmlns:oxy="clr-namespace:OxyPlot.Wpf;assembly=OxyPlot.Wpf"
    d:DesignHeight="450"
    d:DesignWidth="800"
    mc:Ignorable="d">
    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition MinHeight="150" />
            <RowDefinition Height="Auto" />
        </Grid.RowDefinitions>
        <oxy:Plot
            Background="{StaticResource BackgroundBrush}"
            PlotAreaBackground="{StaticResource BackgroundBrush}"
            PlotAreaBorderColor="{Binding Path=Color, Source={StaticResource BorderBrush}}">

            <oxy:Plot.Axes>
                <oxy:LinearAxis
                    AxislineColor="{Binding Path=Color, Source={StaticResource PrimaryBrush}}"
                    EndPosition="1"
                    IsPanEnabled="False"
                    IsZoomEnabled="False"
                    MajorGridlineColor="{Binding Path=Color, Source={StaticResource PrimaryBrush}, Converter={StaticResource SetAlphaToColorConverter}, ConverterParameter=60}"
                    MajorGridlineStyle="LongDash"
                    MaximumPadding="0.1"
                    MinimumPadding="0.1"
                    Position="Left"
                    StartPosition="0"
                    TextColor="{Binding Path=Color, Source={StaticResource PrimaryBrush}}"
                    TicklineColor="{Binding Path=Color, Source={StaticResource SecondaryBrush}}" />

                <oxy:LinearAxis
                    AxislineColor="{Binding Path=Color, Source={StaticResource PrimaryBrush}}"
                    EndPosition="1"
                    IsPanEnabled="False"
                    IsZoomEnabled="False"
                    MajorGridlineColor="{Binding Path=Color, Source={StaticResource PrimaryBrush}, Converter={StaticResource SetAlphaToColorConverter}, ConverterParameter=60}"
                    MajorGridlineStyle="LongDash"
                    MaximumPadding="0.1"
                    MinimumPadding="0.1"
                    Position="Bottom"
                    StartPosition="0"
                    TextColor="{Binding Path=Color, Source={StaticResource PrimaryBrush}}"
                    TicklineColor="{Binding Path=Color, Source={StaticResource SecondaryBrush}}" />
            </oxy:Plot.Axes>
            <oxy:Plot.Series>
                <oxy:LineSeries
                    DataFieldX="FocusPosition"
                    DataFieldY="HFR"
                    ItemsSource="{Binding FocusPoints}"
                    MarkerFill="{Binding Path=Color, Source={StaticResource PrimaryBrush}}"
                    MarkerType="Circle"
                    Color="{Binding Path=Color, Source={StaticResource SecondaryBrush}}" />
            </oxy:Plot.Series>
            <oxy:Plot.Annotations>
                <oxy:LineAnnotation
                    Intercept="{Binding LeftTrend.Offset}"
                    Slope="{Binding LeftTrend.Slope}"
                    Color="{Binding Path=Color, Source={StaticResource ButtonBackgroundBrush}}" />
                <oxy:LineAnnotation
                    Intercept="{Binding RightTrend.Offset}"
                    Slope="{Binding RightTrend.Slope}"
                    Color="{Binding Path=Color, Source={StaticResource ButtonBackgroundBrush}}" />
            </oxy:Plot.Annotations>
        </oxy:Plot>
        <StackPanel
            Grid.Row="1"
            DataContext="{Binding LastAutoFocusPoint}"
            Orientation="Vertical">
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblTime}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding Timestamp}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblPosition}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding Focuspoint.X, StringFormat=\{0:0.00\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblHFR}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding Focuspoint.Y, StringFormat=\{0:0.00\}}" />
                </UniformGrid>
            </Border>
            <Border
                Margin="0,5,0,0"
                BorderBrush="{StaticResource BorderBrush}"
                BorderThickness="0">
                <UniformGrid VerticalAlignment="Center" Columns="2">
                    <TextBlock Text="{ns:Loc LblTemperature}" />
                    <TextBlock Margin="5,0,0,0" Text="{Binding Temperature, StringFormat=\{0:0.00\}}" />
                </UniformGrid>
            </Border>
        </StackPanel>
    </Grid>
</UserControl>